import model.Animal;
import model.Cat;
import model.Dog;
import model.Mammal;

public class App {
    public static void main(String[] args) throws Exception {
        Animal animal1 = new Animal("Money");
        Animal animal2 = new Animal("Cash");
        System.out.println("Animal1: " + animal1.toString());
        System.out.println("Animal2: " + animal2.toString());

        Mammal mammal1 = new Mammal(animal1.getName());
        Mammal mammal2 = new Mammal("My Dieu");
        System.out.println("Mammal1: " + mammal1.toString());
        System.out.println("Mammal2: " + mammal2.toString());

        Cat cat1 = new Cat(animal2.getName());
        Cat cat2 = new Cat("Le Rui");
        System.out.println("Cat1: " + cat1.toString());
        System.out.println("Cat2: " + cat2.toString());
        cat1.greets();

        Dog dog1 = new Dog("bee");
        Dog dog2 = new Dog("Orange");
        System.out.println("Dog1: " + dog1.toString());
        System.out.println("Dog2: " + dog2.toString());
        dog1.greets();
        dog1.greets(dog2);

    }
}
